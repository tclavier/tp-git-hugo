# TP git

## À propos

Vous trouverez ici un TP git pour explorer une partie des capacités de git.

## Le TP

* Identifiez-vous sur le gitlab de l'IUT
* Clonner le projet sur vos postes de développement puis lancer le serveur : `./bin/hugo server --buildDrafts -w` 
* Visitez : http://localhost:1313/

## Site
https://tclavier.gitlab.io/tp-git-hugo/

## Resources

En plus du livre https://git-scm.com/book/fr vous trouverez un grand nombre d'information dans les différents man ainsi que dans ce petit [guide de survie git](https://github.com/tclavier/memo-git/).
